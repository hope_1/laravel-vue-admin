<?php

namespace App\Exceptions\Admin;

use App\Exceptions\Exception;
use App\Modules\Admin\Entities\Log\AdminLoginLog;
use Illuminate\Http\Request;

class AuthException extends Exception
{
    protected $admin_id = 0;
    public function __construct(string $message = "", int $http_status = 401, int $admin_id = 0)
    {
        parent::__construct($message, $http_status);
        $this->admin_id = $admin_id;
    }

    public function render(Request $request)
    {
        if ($request->expectsJson()) {
            // 登录日志
            AdminLoginLog::getInstance()->add($this->admin_id, 0, $this->getMessage());

            return $this->errorJson($this->getMessage(), $this->getCode());
        }
    }
}
