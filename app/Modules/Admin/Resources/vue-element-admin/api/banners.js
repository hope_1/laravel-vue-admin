import request from '@/utils/request'

export function getList(params, get_url = false) {
    var url = `/banners`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    });
}

export function create(data, get_url = false) {
    var url = `/banners/create`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    });
}

export function update(data, get_url = false) {
    var url = `/banners/update`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    });
}

export function setDel(data, get_url = false) {
    var url = `/banners/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    });
}

export function changeFiledStatus(data, get_url = false) {
    var url = `/banners/changeFiledStatus`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}
