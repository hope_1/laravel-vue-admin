import request from '@/utils/request'

export function getList(query, get_url = false) {
    var url = `/configs`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params: query
    })
}

// 获取配置分组与配置类型
export function getConfigGroupType(get_url = false) {
    var url = `/configs/getConfigGroupType`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
    })
}

export function detail(id, get_url = false) {
    var url = `/configs/detail`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params: { config_id:id }
    })
}

export function create(data, get_url = false) {
    var url = `/configs/create`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function update(data, get_url = false) {
    var url = `/configs/update`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}

export function setDel(data, get_url = false) {
    var url = `/configs/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}

export function changeFiledStatus(data, get_url = false) {
    var url = `/configs/changeFiledStatus`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}

// 同步配置文件
export function pushRefreshConfig(get_url = false) {
    var url = `/configs/pushRefreshConfig`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
    })
}
