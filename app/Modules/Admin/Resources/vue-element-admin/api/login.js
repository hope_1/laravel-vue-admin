import request from '@/utils/request'

export function login(data, get_url = false) {
    var url = `/auth/login`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function getInfo(get_url = false) {
    var url = `/auth/me`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post'
    })
}

export function getMenus(get_url = false) {
    var url = `/auth/getRabcList`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post'
    })
}

export function logout(get_url = false) {
    var url = `/auth/logout`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post'
    })
}
