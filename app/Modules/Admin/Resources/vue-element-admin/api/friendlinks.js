import request from '@/utils/request'

export function getList(params, get_url = false) {
    var url = `/friendlinks`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    })
}

export function create(data, get_url = false) {
    var url = `/friendlinks/create`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function update(data, get_url = false) {
    var url = `/friendlinks/update`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}

export function setDel(data, get_url = false) {
    var url = `/friendlinks/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}

export function changeFiledStatus(data, get_url = false) {
    var url = `/friendlinks/changeFiledStatus`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}
