import request from '@/utils/request'

// 文件列表
export function getFileGroup(params, get_url = false) {
    var url = `/getGroupList`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    })
}

export function create(data, get_url = false) {
    var url = `/fileGroup/create`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    });
}

export function update(data, get_url = false) {
    var url = `/fileGroup/update`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    });
}

export function delFileGroup(data, get_url = false) {
    var url = `/fileGroup/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}


