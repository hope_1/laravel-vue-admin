import request from '@/utils/request'

export function getList(params) {
    return request({
        url: 'adminlogs',
        method: 'get',
        params
    })
}

export function setDel(data, get_url = false) {
    var url = `/adminlogs/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}
