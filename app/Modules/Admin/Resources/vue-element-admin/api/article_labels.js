import request from '@/utils/request'

export function getArticleLabelSelect(params) {
    return request({
        url: '/article_labels/getSelectLists',
        method: 'get',
        params
    });
}

export function getList(params, get_url = false) {
    var url = `/article_labels`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    });
}

export function create(data, get_url = false) {
    var url = `/article_labels/create`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    });
}

export function update(data, get_url = false) {
    var url = `/article_labels/update`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    });
}

export function setDel(data, get_url = false) {
    var url = `/article_labels/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    });
}
