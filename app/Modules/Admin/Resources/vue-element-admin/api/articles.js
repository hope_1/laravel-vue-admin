import request from '@/utils/request'

export function getCategoriesSelect() {
    return request({
        url: '/article_categories/getSelectLists',
        method: 'get'
    })
}

export function getList(params, get_url = false) {
    var url = `/articles`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    })
}

export function detail(id, get_url = false) {
    var url = `/articles/detail`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params: { article_id:id }
    })
}

export function create(data, get_url = false) {
    var url = `/articles/create`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function update(data, get_url = false) {
    var url = `/articles/update`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}

export function setDel(data, get_url = false) {
    var url = `/articles/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}

export function changeFiledStatus(data, get_url = false) {
    var url = `/articles/changeFiledStatus`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}
